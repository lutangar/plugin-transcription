import { ChildProcess } from 'child_process';
import { cwd } from 'process';
import { resolve } from 'path';
import { createTranscodingProcess, stopProcess } from './utils';

(async () => {
  let ffmpegProcess: ChildProcess | undefined = undefined;
  try {
    ffmpegProcess = createTranscodingProcess({
      inputFilePath: resolve(cwd(), './data/videos/eye.mp4'),
      outputFilePath: './data/audios/eye.wav',
      sampleRate: 16000,
    });
  } catch (e) {
    stopProcess(ffmpegProcess);
    console.error(e);
  }
})();
