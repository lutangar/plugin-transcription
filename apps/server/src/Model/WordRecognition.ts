import { WordResult } from 'vosk';
import { WordRecognitionInterface } from './WordRecognitionInterface';

export class WordRecognition implements WordRecognitionInterface {
  public conf: number;
  public start: number;
  public end: number;
  public word: string;
  public distanceToNext: number | undefined;

  constructor(wordResult: WordResult, nextWordResult?: WordResult) {
    this.conf = wordResult.conf;
    this.start = wordResult.start;
    this.end = wordResult.end;
    this.word = wordResult.word;
    if (nextWordResult) {
      this.distanceToNext = WordRecognition.silenceDurationBetween(wordResult, nextWordResult);
    }
  }

  private static silenceDurationBetween(wordResult: WordResult, nextWordResult: WordResult) {
    return nextWordResult.start - wordResult.end;
  }

  public isAboveSilenceThreshold(breakThreshold: number) {
    return this.distanceToNext ? this.distanceToNext > breakThreshold : false;
  }
}
