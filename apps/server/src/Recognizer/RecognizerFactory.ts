import { Recognizer } from 'vosk';
import { ModelFactory } from './ModelFactory';

export class RecognizerFactory {
  private modelFactory: ModelFactory;

  constructor(modelFactory: ModelFactory) {
    this.modelFactory = modelFactory;
  }

  createWordsRecognizerFromLanguage({ language = 'en', sampleRate = 16000 }) {
    if (!language) {
      throw new Error(`Couldn't create a words recognizer: undefined language.`);
    }

    const model = this.modelFactory.createFromLanguage(language);
    const recognizer = new Recognizer({ model, sampleRate });
    recognizer.setWords(true);

    return recognizer;
  }
}
