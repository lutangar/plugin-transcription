import { NodeCue } from '../Model/NodeCue';
import { RecognitionResults, WordResult } from 'vosk';

export const WORDS_PER_LINE = 6;

export function extractPartialNodeCues(wordResults: WordResult[], wordsPerLine = WORDS_PER_LINE) {
  console.log('wordResults', wordResults);
  if (!wordResults) {
    return [];
  }

  let i = 0;
  const n = wordResults.length;
  const partialNodeCues = [];
  while (i < n) {
    const chunks = wordResults.slice(i, (i += wordsPerLine));
    if (chunks.length > 0) {
      partialNodeCues.push(
        new NodeCue({
          start: chunks[0].start,
          end: chunks[chunks.length - 1].end,
          text: chunks.map(({ word }) => word).join(' '),
        })
      );
    }
  }

  return partialNodeCues;
}

export function extractNodeCues(nodeCues: NodeCue[], { result: wordResults }: RecognitionResults): NodeCue[] {
  return nodeCues.concat(extractPartialNodeCues(wordResults, WORDS_PER_LINE));
}
