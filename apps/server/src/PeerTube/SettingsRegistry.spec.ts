import { Setting, SettingsRegistry } from './SettingsRegistry';
import { pluginStorageManager } from '../__mocks__/PeerTube/PluginStorageManager';
import { pluginSettingsManager, registerPluginSetting } from '../__mocks__/PeerTube/pluginSettings';

describe('SettingsRegistry', () => {
  test('register', async () => {
    const settingsRegistry = new SettingsRegistry(
      pluginSettingsManager,
      registerPluginSetting,
      pluginStorageManager,
      console
    );

    await settingsRegistry.register(
      new Setting({
        name: 'mySetting',
        type: 'input',
        label: '',
        private: false,
        value: 'heyHi',
      })
    );

    expect(registerPluginSetting).toBeCalled();

    const storageKey = settingsRegistry.getStorageKey('mySetting');
    expect(storageKey).toEqual('plugin_setting_mySetting');

    const settingName = settingsRegistry.getNameFromStorageKey(storageKey);
    expect(settingName).toEqual('mySetting');

    let mySettingValue = await settingsRegistry.get('mySetting');
    expect(mySettingValue).toEqual('heyHi');

    await settingsRegistry.set('mySetting', 'heyHo');
    // API is weird and should be change once PeerTube changes.
    mySettingValue = await settingsRegistry.getPrevious('mySetting');
    expect(mySettingValue).toEqual('heyHo');
  });
});
