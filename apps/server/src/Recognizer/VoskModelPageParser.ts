import { VoskModelDescriptor } from '../Model/VoskModelDescriptor';
import { LoggerInterface } from '../Model/LoggerInterface';
import { get, extractLanguage } from '../utils';

export class VoskModelPageParser {
  private modelPageUrl: string;
  private logger: LoggerInterface;
  private availableLanguages: string[];
  public availableModels: VoskModelDescriptor[];

  constructor(modelPageUrl: string, logger: LoggerInterface, availableLanguages: string[]) {
    this.modelPageUrl = modelPageUrl;
    this.logger = logger;
    this.availableLanguages = availableLanguages;
    this.availableModels = [];
  }

  isLanguageAvailable(language: string) {
    return this.availableLanguages.indexOf(language) !== -1;
  }

  isModelLanguageAvailable({ lang: locale }: VoskModelDescriptor) {
    return this.isLanguageAvailable(extractLanguage(locale));
  }

  async parse(): Promise<VoskModelDescriptor[]> {
    this.logger.info(`Fetching Vosk models from ${this.modelPageUrl}...`);

    const responseBody = await get(this.modelPageUrl);

    const allAvailableModels = JSON.parse(responseBody as string) as VoskModelDescriptor[];
    this.availableModels = allAvailableModels.filter(this.isModelLanguageAvailable.bind(this));

    this.logger.info(
      `Found ${this.availableModels.length.toString()} valid models for languages: ${this.getModelsLanguages().join(
        ', '
      )}.`
    );

    return this.availableModels;
  }

  getModelsLanguages() {
    return this.availableModels
      .map(({ lang: locale }) => extractLanguage(locale))
      .filter((value, index, self) => self.indexOf(value) === index);
  }
}
