# PeerTube

A video is uploaded to the `upload-resumable` endpoint by post/put
Internally it uses uploadx lib to upload it

notifyOnFinishedVideoImport

    // Create transcoding jobs?
    if (video.state === VideoState.TO_TRANSCODE) {
      await addOptimizeOrMergeAudioJob(videoImportUpdated.Video, videoFile, videoImport.User)
    }

It's then transcoded, 

> Débit binaire 128Kbps
> Disposition des chaînes mono
> CodecAAC (Advanced Audio Coding)
> Profil LC
> Fréquence d'échantillonnage 44100

VideoFile
getFileStaticPath
