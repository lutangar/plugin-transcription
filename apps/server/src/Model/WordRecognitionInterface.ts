import { WordResult } from 'vosk';

export interface WordRecognitionInterface extends WordResult {
  distanceToNext?: number;
}
