import { PluginStorageManager } from '@peertube/peertube-types';
import { AbstractRepository } from './AbstractRepository';
import { LoggerInterface, VoskModelDescriptor } from '../Model';
import { extractLanguage } from '../utils';
import { VoskModelPageParser } from '../Recognizer';

export class LanguageModelRepository extends AbstractRepository {
  private models: VoskModelDescriptor[];
  private static STORAGE_KEY = 'models';
  private modelPageParser: VoskModelPageParser;

  constructor(storage: PluginStorageManager, voskModelPageParser: VoskModelPageParser, logger: LoggerInterface) {
    super(storage, logger);

    this.modelPageParser = voskModelPageParser;
    this.models = [];
  }

  getModelByName(name: string | boolean) {
    return this.models.find(({ name: modelName }) => name === modelName);
  }

  getModelsByLanguage(language: string) {
    return this.models.filter(({ lang: locale }) => language === extractLanguage(locale));
  }

  async addModel(model: VoskModelDescriptor) {
    await this.storage.storeData(LanguageModelRepository.STORAGE_KEY, this.models.concat([model]));
  }

  getModels() {
    return this.models;
  }

  async load() {
    this.models = await this.modelPageParser.parse();

    await this.store(this.models);

    return this.models;
  }

  async store(models: VoskModelDescriptor[]) {
    await this.storage.storeData(LanguageModelRepository.STORAGE_KEY, models);
    this.models = models;
  }

  findSmallestModelForLanguage(searchedLanguage: string) {
    return this.models
      .sort(({ size: sizeA }, { size: sizeB }) => sizeA - sizeB)
      .find(({ lang: locale }) => extractLanguage(locale) === searchedLanguage);
  }
}
