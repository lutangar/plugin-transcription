import { VoskModelType } from './VoskModelDescriptor';

export interface LanguageModelDescriptor {
  lang: string;
  lang_text: string;
  md5: string;
  name: string;
  obsolete: boolean;
  size: number;
  size_text: string;
  type: VoskModelType;
  url: string;
  version: string;
}
