import { exit } from 'process';
import { parentPort, getEnvironmentData, threadId } from 'worker_threads';
import { resolve } from 'path';
import { TranscriptionRequestProcessor } from './Transcription/TranscriptionRequestProcessor';
import { ModelFactory, RecognizerFactory } from './Recognizer';
import { SubtitleGenerator } from './Subtitle/SubtitleGenerator';
import { createWorkerLogger } from './utils/logger';
import { isTerminationRequest } from './Transcription/TerminationRequest';

if (!parentPort) {
  exit(1);
}

const dataPath = getEnvironmentData('dataPath') as string;
const logger = createWorkerLogger({
  level: 'debug',
  filename: resolve(dataPath, `worker-${threadId}.log`),
  threadId,
});

try {
  const modelsPath = resolve(dataPath, 'models');
  const captionsPath = resolve(dataPath, 'captions');
  const recognizerFactory = new RecognizerFactory(new ModelFactory(modelsPath));
  const subtitleGenerator = new SubtitleGenerator(recognizerFactory, logger);
  const requestProcessor = new TranscriptionRequestProcessor(parentPort, subtitleGenerator, captionsPath, logger);
  parentPort.on('message', (message) => {
    if (isTerminationRequest(message)) {
      logger.info(`Received termination request via ${message.signal} from parent thread.`);
    } else {
      requestProcessor.process.call(requestProcessor, message);
    }
  });
} catch (e) {
  logger.log({ level: 'error', message: e as string });
  parentPort.emit('messageerror', e);
}
