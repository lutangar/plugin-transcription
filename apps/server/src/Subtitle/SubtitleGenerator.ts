import { ChildProcess } from 'child_process';
import { Format, stringify } from 'subtitle';
import { createWriteStream } from 'fs-extra';
import { pipeline } from 'stream';
import { createTranscodingProcess, stopProcess, guessFormatFromFileExtension } from '../utils';
import { RecognizerFactory } from '../Recognizer';
import { NodeCueTransform } from './NodeCueTransform';
import { LoggerInterface } from '../Model';

export interface CaptionGenerationParameters {
  inputFilePath: string;
  language?: string;
  sampleRate?: number;
  format?: Format;
  breakThreshold?: number;
}

export interface CaptionFileGenerationParameters extends CaptionGenerationParameters {
  outputFilePath: string;
}

export class SubtitleGenerator {
  private recognizerFactory: RecognizerFactory;
  private readonly logger: LoggerInterface;

  constructor(recognizerFactory: RecognizerFactory, logger: LoggerInterface) {
    this.recognizerFactory = recognizerFactory;
    this.logger = logger;
  }

  public generate({
    format = 'WebVTT',
    inputFilePath,
    language = 'en',
    sampleRate = 16000,
    breakThreshold = 0.3,
  }: CaptionGenerationParameters) {
    let ffmpegProcess: ChildProcess | undefined = undefined;
    try {
      ffmpegProcess = createTranscodingProcess({
        inputFilePath,
        sampleRate,
        logger: this.logger,
      });
      const recognizer = this.recognizerFactory.createWordsRecognizerFromLanguage({
        language,
        sampleRate,
      });

      if (!ffmpegProcess.stdout) {
        throw new Error('ffmpegProcess.stdout is undefined.');
      }

      return pipeline(
        ffmpegProcess.stdout,
        new NodeCueTransform(recognizer, breakThreshold, this.logger),
        stringify({ format: format }),
        (e) => {
          if (e) {
            this.logger.log({ level: 'error', message: e });
          }
          stopProcess(ffmpegProcess, this.logger);
        }
      );
    } catch (e) {
      stopProcess(ffmpegProcess);
      this.logger.log({ level: 'error', message: e });
      throw e;
    }
  }

  public generateToFile({
    inputFilePath,
    outputFilePath,
    language = 'en',
    sampleRate = 16000,
  }: CaptionFileGenerationParameters) {
    const captionStream = this.generate({
      inputFilePath,
      language,
      sampleRate,
      format: guessFormatFromFileExtension(outputFilePath),
    });

    if (!captionStream) {
      throw new Error('Caption stream is undefined.');
    }

    return pipeline(captionStream, createWriteStream(outputFilePath), (e) => {
      if (e) {
        this.logger.error(e.message, e);
      }
    });
  }
}
